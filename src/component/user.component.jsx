import React, { Component } from 'react'
import HOC1 from './hoc/hoc1.component'

const User=({data})=>
{

    return (
        <div>
            User
            <h1>Name {data.name}</h1>
            <h1>Age {data.age}</h1>
            <h1>Phone {data.phone}</h1>
        </div>
    )
}
export default HOC1(User)