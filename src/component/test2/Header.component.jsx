import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './style.css'

const Header =()=>
{
    return(
        <div className="header">       
            <ul>
                <li><Link to='/'>Home</Link></li>
                <li><Link to='/userlist'>User List</Link></li>
                <li><Link to='/calculator'>Calculator</Link></li>
            </ul>
        </div>
    )
}
export default Header