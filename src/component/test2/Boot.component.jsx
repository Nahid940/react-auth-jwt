import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from './Home.component'
import User from './users.component'
import Calculator from './calculator.component'
import UserDetails from './user-details.component'

const Boot=()=>
{
    return(
        <div>
            <Switch>
                <Route exact path='/' component={Home}/>
                <Route exact path='/userlist' component={User}/>
                <Route path='/calculator' component={Calculator}/>
                <Route exact path="/user/:id" component={UserDetails}/>
            </Switch>
        </div>
    )
}
export default Boot