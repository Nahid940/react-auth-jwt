import React, { Component } from 'react'
import { Link, Route } from 'react-router-dom'
import UserDetails from './user-details.component'

const User=()=>
{
    return(
        <div>
            <table>
                <thead>
                    <tr>
                        <th>Sl</th>
                        <th>Name</th>
                        <th>ID</th>
                        <th>Phone</th>
                        <th>Email</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><Link to="/user/1">1</Link></td>
                        <td>ASD</td>
                        <td>523</td>
                        <td>243125</td>
                        <td>e@mail.com</td>
                    </tr>
                    <tr>
                        <td><Link to="/user/2">2</Link></td>
                        <td>ASD</td>
                        <td>523</td>
                        <td>243125</td>
                        <td>e@mail.com</td>
                    </tr>
                    <tr>
                        <td><Link to="/user/3">3</Link></td>
                        <td>ASD</td>
                        <td>523</td>
                        <td>243125</td>
                        <td>e@mail.com</td>
                    </tr>
                    <tr>
                        <td><Link to="/user/4">4</Link></td>
                        <td>ASD</td>
                        <td>523</td>
                        <td>243125</td>
                        <td>e@mail.com</td>
                    </tr>
                    <tr>
                        <td><Link to="/user/5">5</Link></td>
                        <td>ASD</td>
                        <td>523</td>
                        <td>243125</td>
                        <td>e@mail.com</td>
                    </tr>
                    <tr>
                        <td><Link to="/user/6">6</Link></td>
                        <td>ASD</td>
                        <td>523</td>
                        <td>243125</td>
                        <td>e@mail.com</td>
                    </tr>
                    <tr>
                        <td><Link to="/user/7">7</Link></td>
                        <td>ASD</td>
                        <td>523</td>
                        <td>243125</td>
                        <td>e@mail.com</td>
                    </tr>
                    <tr>
                        <td><Link to="/user/8">8</Link></td>
                        <td>ASD</td>
                        <td>523</td>
                        <td>243125</td>
                        <td>e@mail.com</td>
                    </tr>
                    <tr>
                        <td><Link to="/user/9">9</Link></td>
                        <td>ASD</td>
                        <td>523</td>
                        <td>243125</td>
                        <td>e@mail.com</td>
                    </tr>
                    <tr>
                        <td><Link to="/user/10">10</Link></td>
                        <td>ASD</td>
                        <td>523</td>
                        <td>243125</td>
                        <td>e@mail.com</td>
                    </tr>
                    <tr>
                        <td><Link to="/user/11">11</Link></td>
                        <td>ASD</td>
                        <td>523</td>
                        <td>243125</td>
                        <td>e@mail.com</td>
                    </tr>
                    <tr>
                        <td><Link to="/user/12">12</Link></td>
                        <td>ASD</td>
                        <td>523</td>
                        <td>243125</td>
                        <td>e@mail.com</td>
                    </tr>
                    <tr>
                        <td><Link to="/user/13">13</Link></td>
                        <td>ASD</td>
                        <td>523</td>
                        <td>243125</td>
                        <td>e@mail.com</td>
                    </tr>
                    <tr>
                        <td><Link to="/user/14">14</Link></td>
                        <td>ASD</td>
                        <td>523</td>
                        <td>243125</td>
                        <td>e@mail.com</td>
                    </tr>
                    <tr>
                        <td><Link to="/user/15">15</Link></td>
                        <td>ASD</td>
                        <td>523</td>
                        <td>243125</td>
                        <td>e@mail.com</td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}

export default User