import React, { Component,useState } from 'react'

const Calculator=()=>
{
    const [values,setValues]=useState({
        value1:0,
        value2:0,
        type:0
    })

    const [sum,setResult]=useState(0)

    const handleChange=({target})=>{
        const {name,value}=target
        setValues({
            ...values,
            [name]:value
        })
    }

    const handleSubmit=(e)=>
    {
        e.preventDefault()
        const {value1,value2,type}=values
        var result=0;
        const types=parseInt(type)
        switch (types){
            case 1:
                result=parseFloat(value1)+parseFloat(value2);
                break;
            case 2:
                result=parseFloat(value1)-parseFloat(value2);
                break;
            case 3:
                result=parseFloat(value1)*parseFloat(value2);
                break;
            case 4:
                result=parseFloat(value1)/parseFloat(value2);
                break;
        }

        setResult(result)
    }

    return(
        <div>
            <h5>Calculate Values</h5>
            <form action="" method="post" onSubmit={handleSubmit}>
                <input type="text" name="value1" onChange={handleChange} value={values.value1} placeholder="Value 1"/>
                <input type="text" name="value2"  onChange={handleChange}  value={values.value2} placeholder="Value 2"/>
                <select name="type" id="" onChange={handleChange} value={values.type}>
                    <option value="0">Select</option>
                    <option value="1">Sum</option>
                    <option value="2">Subtract</option>
                    <option value="3">Multiply</option>
                    <option value="4">Devide</option>
                </select>
                <button type='submit'>Calculate</button>
            </form>
            <span>The Result is {sum}</span>
        </div>
    )
}

export default Calculator