import React, { Component } from 'react'
import HOC1 from './hoc/hoc1.component'

const Employee=({data})=>
{
    return(
        <div>
            <h1>Employee</h1>
            <h1>{data.name}</h1>
            <h1>{data.age}</h1>
            <h1>{data.phone}</h1>
        </div>
    )
}

export default HOC1(Employee)

