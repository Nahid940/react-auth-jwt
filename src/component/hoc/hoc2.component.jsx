import React, { Component } from 'react'

const HOC2=(WrappedComponent)=>
{
    class HOC2 extends Component{
        constructor(props)
        {
            super(props)
            this.state={
                name:"Nahid",
                ID:25423455
            }
        }

        render()
        {
            return <WrappedComponent data={this.state} {...this.props}/>
        }
    }
    return HOC2
}

export default HOC2