import React, { Component } from 'react'
import { render } from '@testing-library/react'

const HOC1=(WrappedComponent)=>
{
    class HOC1 extends Component {

        constructor(props)
        {
            super(props)
            this.state={
                data:
                    {
                        'name':'X',
                        'age':20,
                        'phone':125314
                    }
                
            }
        }
        render()
        {
            return <WrappedComponent data={this.state.data} {...this.props} />
        }
    }

    return HOC1;
}

export default HOC1