import React, { Component } from 'react'
import HOC2 from './hoc/hoc2.component'

const Student=({data})=>
{
    return(
        <div>
            <h3>Student</h3>
            Name: {data.name}
            ID: {data.ID}
        </div>
    )
}

export default HOC2(Student)

