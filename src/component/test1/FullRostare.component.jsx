import React, { Component } from 'react'
import { Link } from 'react-router-dom'

const FullRoster=()=>
{
    return(
        <div>
            <li>
                <Link to="/roster/1">Player 1</Link>
                <Link to="/roster/2">Player 2</Link>
                <Link to="/roster/3">Player 3</Link>
                <Link to="/roster/4">Player 4</Link>
                <Link to="/roster/5">Player 5</Link>
                <Link to="/roster/6">Player 6</Link>
            </li>
        </div>
    )
}
export default FullRoster;