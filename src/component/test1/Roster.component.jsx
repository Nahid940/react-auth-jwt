import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import FullRoster from './FullRostare.component'
import Player from './Player.component'

const Roster=()=>
{
    return(
        <div>
            <h3>This is Roster Page</h3>
            <Switch>
                <Route exact path="/roster" component={FullRoster}/>
                <Route path="/roster/:number" component={Player}/>
            </Switch>
        </div>
    )
}
export default Roster