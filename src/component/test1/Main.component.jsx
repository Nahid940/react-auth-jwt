import React, { Component } from 'react'
import { Switch,Route} from 'react-router-dom'
import Roster from './Roster.component'
import Schedule from './Schedule.component'
import Home from './Home.component'

const Main=()=>
{
    return (
        <main>
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/roster" component={Roster} />
                <Route path="/schedule" component={Schedule} />
            </Switch>
        </main>
    )
}
export default Main