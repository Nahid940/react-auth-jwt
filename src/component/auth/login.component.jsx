import React, { Component,useState } from 'react'
import axios from 'axios'
import {connect} from 'react-redux'

import {authenticateUser} from '../../redux/auth/auth.action'

const Login=(props)=>
{
    const[info,setInfo]=useState({
        username:'',
        password:''
    })

    const [error,setError]=useState('')

    const handleChange=(e)=>{
        const {name,value}=e.target
        setInfo({
            ...info,
            [name]:value
        })
    }

    const base_url="http://localhost/phpreact/files/login.php?login";

    const handleSubmit=(e)=>
    {
        e.preventDefault()
        const {username,password}=info
        if(username && password)
        {
            axios.post(base_url,JSON.stringify(info)).then(res=>{
                // setInfo({
                //     username:'',
                //     password:''
                // })
                if(res.data.token)
                {
                    props.authenticateUser(res.data)
                    props.history.push('/')
                }else
                {
                    setError("Invalid Login!! Try Again !!")
                }
            })
        }
    }

    return(
        <div>
            <h3>Login to the system</h3>
            {error?error:''}
            <form action="" method="post" onSubmit={handleSubmit}>
                Username <input type="text" value={info.name} onChange={handleChange} placeholder="Username" name="username"/>
                Password <input type="password" value={info.password} onChange={handleChange} placeholder="Password" name="password"/>
                <input type="submit" value="Login"/>
            </form>
        </div>
    )
}
const mapDispacthToProprs=(dispatch)=>
({
    authenticateUser:user=>dispatch(authenticateUser(user))
})

export default connect(null,mapDispacthToProprs)(Login)