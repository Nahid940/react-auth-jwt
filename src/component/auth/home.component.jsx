import React, { Component } from 'react'
import Boot from './bootfile.component'
import AuthLink from './auth.link.component'
import {connect} from 'react-redux'

const Home=({user_info})=>
{
    return(
        <div>
            System Maina 
            {user_info?`Welcome Mr ${user_info.user_name}`:''}
        </div>
    )
}

const mapStateToProps=(state)=>({
    user_info:state.auth.auth_user
})

export default connect(mapStateToProps)(Home)