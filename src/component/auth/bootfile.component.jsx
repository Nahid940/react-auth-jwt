import React, { Component } from 'react'
import {Route, Switch} from 'react-router-dom'
import Login from './login.component'
import Registration from './registration.component'
import Home from './home.component'
import Users from './users.component'
const Boot=()=>
{
    return(
        <Switch>
            <Route exact path="/" component={Home}/>
            <Route path="/login" component={Login}/>
            <Route path="/register" component={Registration}/>
            <Route path="/users" component={Users}/>
        </Switch>
    )
}
export default Boot