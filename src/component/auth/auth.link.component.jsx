import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import {connect} from 'react-redux'

const AuthLink=({token})=>
{
    return(
        <div>
            <Link to='/'>Home</Link>
            <Link to='/login'>Login</Link>
            <Link to='/register'>Registration</Link>
            {token?<Link to='/users'>Users</Link>:''}
        </div>
    )
}

const mapStateToProps=(state)=>
({
    token:state.auth.auth_user
})

export default connect(mapStateToProps)(AuthLink)