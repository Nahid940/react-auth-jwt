import React, { Component,useState } from 'react'
import axios from 'axios'
import './style.css'

const Registration=(props)=>
{
    const [info,setInfo]=useState({
        username:'',
        password:''
    })
    const [success,setSuccess]=useState("")
    const [error,setError]=useState("")
    const [loading,setLoading]=useState(false)
    const handleChange=(e)=>{
        const {name,value}=e.target
        setInfo({
            ...info,
            [name]:value
        })
    }
    const base_url="http://localhost/phpreact/files/register.php?register";
    const handleSubmit=(e)=>{
        e.preventDefault()
        if(info.username && info.password){
            axios.post(base_url,JSON.stringify(info)).then(res=>{
                if(res.status==200){
                    setSuccess("Registration successful !! Now you can login")
                    setTimeout(function(){
                        props.history.push('/login')
                    },5000)
                    setInfo({
                        username:'',
                        password:''
                    })
                    setLoading(true)
                }
            })
        }else{
            setError("Username and Password is required !!")
        }
    }

    return(
        <div>
            <h3>Get yourself registered</h3>
            {success?<h4>{success}</h4>:''}
            {error?<h4 style={{color:'red'}}>{error}</h4>:''}

            {/* {loading?<div className="loader"></div>:''} */}

            <form action="" method="post" onSubmit={handleSubmit}>
                <label htmlFor="username">Username</label>
                <input type="text" placeholder="Username" name="username" value={info.username} onChange={handleChange}/>
                <label htmlFor="password">Password</label>
                <input type="password" placeholder="Password" name="password" value={info.password} onChange={handleChange}/>
                <input type="submit" value="Register"/>
            </form>
            
        </div>
    )
}

export default Registration