import React, { Component,useEffect,useState } from 'react'
import {connect} from 'react-redux'
import axios from 'axios'

const Users=({mytoken})=>
{
    const [users,setUsers]=useState([])
    const [allow,setAllow]=useState(false)

    const base_url="http://localhost/phpreact/files/users.php?users";

    useEffect(()=>{
        const {token}=mytoken
            axios.post(base_url,
            {
                headers: { Authorization: "Bearer " + token }
            }
            ).then(res=>{
                console.log(res)
                // if(res.data.status===200)
                // {
                    setAllow(true)
                    setUsers(...users,res.data.users)
                // }else{
                //     setAllow(false)
                // }
            })
    },[])

    return(
        <div>
            <h4>Users List</h4>
            {allow?<h2>Allowed</h2>:'Disallowed'}
            <table>
                <thead>
                    <tr>
                        <th>Sl</th>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {users?
                    users.map(user=>
                     <tr key={user.user_id}>
                        <td>{user.user_id}</td>
                        <td>{user.user_name}</td>
                    </tr>)
                    :''}
                </tbody>
            </table>
        </div>
    )
}

const mapStateToProps=(state)=>({
    mytoken:state.auth.auth_user
})

export default connect(mapStateToProps)(Users)