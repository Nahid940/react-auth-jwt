const INITIAL_STATE={
    auth_user:null
}

const authReducer=(state=INITIAL_STATE,action)=>
{
    switch(action.type){
        case 'USER_LOGIN':
            return({
                ...state,
                auth_user:action.payload
            })
        default :
        return state
    }
}
export default authReducer